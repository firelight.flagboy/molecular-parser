# Molecular Parser

## Description

For a given chemical formula represented by a string, write a function that counts
the number of atoms of each element contained in the molecule and returns an
object where keys correspond to atoms and values to the number of each atom in
the molecule.

For example:

```js
var water = 'H2O'
parse_molecule(water)                 // return {'H': 2, 'O': 1}

var magnesium_hydroxide = 'Mg(OH)2'
parse_molecule(magnesium_hydroxide)   // return {'Mg': 1, 'O': 2, 'H': 2}

var fremy_salt = 'K4[ON(SO3)2]2'
parse_molecule(fremy_salt)             // return {'K': 4, 'O': 14, 'N': 2, 'S': 4}
```

As you can see, some formulas have brackets in them. The index outside the brackets
tells you that you have to multiply count of each atom inside the bracket on this
index. For example, in Fe(NO3)2 you have one iron atom, two nitrogen atoms and
six oxygen atoms.

Note that brackets may be round, square or curly and can also be nested. Index after the braces is optional.

## Usage

```
$> node molecul-parser-app.js "(C6H10O5)16"
{ C: 96, H: 160, O: 80 }
```