var molecular = require('../src/molecular-parser.js');
var chai = require('chai');
var expect = chai.expect;

var parse_molecule = molecular.parse_molecule

describe('parse molecule', () => {
	it('describe H2O', () => {
		let water = 'H2O';
		let res = parse_molecule(water);	// return {'H': 2, 'O': 1}
		expect(res).to.deep.equal({
			'H': 2,
			'O': 1
		});
	});
	it('describe Mg(OH)2', () => {
		let magnesium_hydroxide = 'Mg(OH)2';
		let res = parse_molecule(magnesium_hydroxide);	// return {'Mg': 1, 'O': 2, 'H': 2}
		expect(res).to.deep.equal({
			'Mg': 1,
			'O': 2,
			'H': 2
		});
	});
	it('describe K4[ON(SO3)2]2', () => {
		let fremy_salt = 'K4[ON(SO3)2]2';
		let res = parse_molecule(fremy_salt);	// return {'K': 4, 'O': 14, 'N': 2, 'S': 4}
		expect(res).to.deep.equal({
			'K': 4,
			'O': 14,
			'N': 2,
			'S': 4
		})
	});
	it('describe Fe(NO3)2', () => {
		let mol = 'Fe(NO3)2';
		let res = parse_molecule(mol);
		expect(res).to.deep.equal({
			'Fe': 1,
			'N': 2,
			'O': 6
		})
	})
	it('describe Mg4[O2N(SuU)6Ar4]19', () => {
		let mol = 'Mg4[O2N(Su2U)6Ar4]19';
		let res = parse_molecule(mol);
		expect(res).to.deep.equal({
			'Mg': 4,
			'O': 38,
			'N': 19,
			'Su': 228,
			'U': 114,
			'Ar': 76
		})
	})
	it('decribe ((H)2O)20', () => {
		let mol = '((H)2O)20';
		let res = parse_molecule(mol);
		expect(res).to.deep.equal({
			'H': 40,
			'O': 20
		});
	});
});


describe('parser error handler molecule name', () => {
	it('when bad parenthesis', () => {
		let mol = '{}'
		let res = parse_molecule(mol)
		expect(res).to.deep.equal({ error: 'bad char in molecule : {}' })
	})
	it('when other char not letter or number', () => {
		let mol = '\''
		let res = parse_molecule(mol)
		expect(res).to.deep.equal({ error: 'bad char in molecule : \''})
	})
	it('test bad named mol gM', () => {
		let mol = 'gM'
		let res = parse_molecule(mol)
		expect(res).to.deep.equal({ error: 'bad char : g' })
	})
	it('test bad named mol MgxO', () => {
		let mol = 'MgxO'
		let res = parse_molecule(mol)
		expect(res).to.deep.equal({ error: 'bad named Atom : Mgx' })
	})
	it('test good named mol Mg(O)2', () => {
		let mol = 'Mg(O)2'
		let res = parse_molecule(mol)
		expect(res).to.not.have.property('error')
		expect(res).to.deep.equal({
			'Mg': 1,
			'O': 2
		})
	})
})

describe('parser error handler pth', () => {
	it('test parse error when no ending pth Mg(O2', () => {
		let mol = 'Mg(O2'
		let res = parse_molecule(mol)
		expect(res).to.deep.equal({ error: 'no closing pth' })
	})
	it('test parse error when no ending pth Mg[O2', () => {
		let mol = 'Mg[O2'
		let res = parse_molecule(mol)
		expect(res).to.deep.equal({ error: 'no closing pth' })
	})
	it('test parse error when no beging pth is found MgO2)', () => {
		let mol = 'MgO2)'
		let res = parse_molecule(mol)
		expect(res).to.deep.equal({ error: ') not a word'})
	})
	it('test parse error when no beging pth is found MgO2]', () => {
		let mol = 'MgO2]'
		let res = parse_molecule(mol)
		expect(res).to.deep.equal({ error: '] not a word'})
	})
})
