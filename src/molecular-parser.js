
let closing = {
	'(': ')',
	'[': ']'
}

function checkParasiteChar(str) { // check the mol string
	let reg = /^[A-Za-z0-9\(\)\[\]]*$/; // check if parasite char

	if (!reg.test(str))
		return ({ error: 'bad char in molecule : ' + str });
	return ({});
}

function spliter(str) { // split the mol into a array
	let spliter = [];

	for (let i = 0; i < str.length; i++) {
		if (/[A-Z]/.test(str[i])) { // if atom name
			let name = str[i];
			while (/[a-z]/.test(str[i + 1]) && i + 1 < str.length) {
				name += str[1 + i++];
			}
			// check if the name of the Atom if only a upper and lower case letter
			if (!/^[A-Z][a-z]?$/.test(name))
				return ({ error: 'bad named Atom : ' + name });
			spliter.push(name);
		}
		else if (/[1-9]/.test(str[i])) { // if number
			let num = str[i];
			while (/[0-9]/.test(str[i + 1]) && i + 1 < str.length) {
				num += str[1 + i++];
			}
			spliter.push(num);
		}
		else if (/[\(\[\]\)]/.test(str[i])) {// if parentheses
			spliter.push(str[i]);
		}
		else {
			return ({ error: 'bad char : ' + str[i]});
		}
	}
	return ({ spliter });
}

function addAtomToMolObj(mol, atm, coef) { // add atome to mol
	if (atm in mol)
		mol[atm] += coef;
	else
		mol[atm] = coef;
}

function parseInsidePth(mol, arr, mul) { // parser the mol inside pth
	let end = arr.lastIndexOf(closing[arr[0]]);
	let coef = 1; // coef of the global pth

	if (end === -1) // if no ending pth
		return ({ error : 'no closing pth'});
	if (/\d/.test(arr[end + 1])) { // get the coef of the pth if exist
		coef = Number(arr[end + 1]);
		arr.splice(end, 2);
	}
	else
		arr.splice(end, 1);
	end -= 2;
	arr.splice(0, 1);
	return (parseOverArray(mol, arr, end, coef * mul));
}

function parseOverArray(mol, arr, end, coef) {
	for (let j = 0; j <= end && arr.length > 0; j++) {
		if (/[\(\[]/.test(arr[0])) {
			let ret = parseInsidePth(mol, arr, coef); // parse the pth
			if ('error' in ret) // if error
				return (ret);
			continue;
		}
		if (!/\w/.test(arr[0])) { // if not a word
			return ({ error: arr[0] + ' not a word' });
		}
		if (/\d/.test(arr[1])) { // if the next element is a digit get the coef
			let num = Number(arr[1]);
			addAtomToMolObj(mol, arr[0], num * coef);
			j++; // skip the number
			arr.splice(0, 2);
		}
		else { // else add with base coef
			addAtomToMolObj(mol, arr[0], 1 * coef);
			arr.splice(0, 1);
		}
	}
	return (mol);
}

function parse_molecule(molecule) { // main function
	let retCheck = checkParasiteChar(molecule);
	if ('error' in retCheck) // if the basic check found an error
		return (retCheck);
	let retLexer = spliter(molecule);
	if ('error' in retLexer) // if the lexer have found an error
		return (retLexer);
	return (parseOverArray({}, retLexer.spliter, retLexer.spliter.length, 1));
}

module.exports = {
	parse_molecule: parse_molecule,
	parse_molecule_fc: {
		parse_molecule: parse_molecule,
		parser: parseOverArray,
		parser_pth: parseInsidePth,
		lexer: spliter,
		checker: checkParasiteChar
	}
}
